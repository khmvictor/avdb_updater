// Modules to control application life and create native browser window
const { BrowserWindow,
  Menu,
  Tray,
  ipcMain,
  dialog } = require('electron')
const path = require('path')
const global = require('./global');
const { templateMenu, contextMenu } = require('./mainMenu');
const { downloadService } = require('./download');
const { unZipService } = require('./unzip');
const ProgressBar = require('electron-progressbar');
const { fileSizeService } = require('./filesize');
const findRemoveSync = require('find-remove');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
///let mainWindow

let progressBar;

function createWindow() {
  // Create the browser window.
  global.win = new BrowserWindow({
    width: 1000,
    height: 750,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true
    }
  });

  const menu = Menu.buildFromTemplate(templateMenu);
  Menu.setApplicationMenu(menu);

  const iconPath = path.join(__dirname, 'assets/icon.png');
  appIcon = new Tray(iconPath);
  var contex_menu = Menu.buildFromTemplate(contextMenu);
  appIcon.setToolTip('ESET updater.');
  appIcon.setContextMenu(contex_menu);

  // and load the index.html of the app.
  global.win.loadFile('index.html');

  // Open the DevTools.
  //global.win.webContents.openDevTools();

  ipcMain.on('download-start', downloadUpgrades);

  ipcMain.on('app-close', closeApp);

  //ipcMain.on('download_completed', setProgressbarCompleted);

  //downloadUpgrades();

  // Emitted when the window is closed.
  global.win.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    global.win = null
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
global.app.on('ready', createWindow)

// Quit when all windows are closed.
global.app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') global.app.quit()
})

global.app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (global.win === null) createWindow()
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

function closeApp() {
  console.log('App will close now...');
  global.win.close();
}

function downloadUpgrades() {

  downloadService.downloadFile({
    file_name: 'offline_update_eav.zip',
    db_ulr: 'http://soft.ivanovo.ac.ru/updates/eset/',
    local_folder: 'D:\\Temp\\eset_updates\\',
    onStarted: (totalBytes) => {

      progressBar = new ProgressBar({
        indeterminate: false,
        text: 'Preparing data...',
        title: 'Wait please, file is downloading ...',
        initialValue: 0,
        maxValue: totalBytes,
        browserWindow: {
          parent: global.win,
          width: 500,
          icon: 'assets/icon.png',
          webPreferences: {
            nodeIntegration: true
          }
        }
      }, global.app);

      progressBar
        .on('completed', function () {

          console.log(`Progress: Downloading is completed.`);
          //progressBar.value = progressBar.getOptions().maxValue;  
          progressBar.detail = 'Downloading is completed.';
          setTimeout(function () {
            progressBar.close();
            progressBar = null;
          }, 1000);

        })
        .on('aborted', function (value) {
          console.info(`aborted... ${value}`);
        })
        .on('progress', function (value) {
          //console.log(`Progress: Value ${fileSizeService.filesize(value)} out of ${fileSizeService.filesize(progressBar.getOptions().maxValue)}...`);
          progressBar.detail = `Progress: ${((value * 100) / progressBar.getOptions().maxValue).toFixed(2)} %. Downloaded ${fileSizeService.filesize(value)} out of ${fileSizeService.filesize(progressBar.getOptions().maxValue)} ...`;
        });

    },
    onProgress: (received, total) => {

      var percentage = (received * 100) / total;
      global.win.setProgressBar(percentage / 100);

      if (!progressBar.isCompleted()) {
        progressBar.text = 'Downloading file...';
        progressBar.value = received;
      }
      //console.log(percentage.toFixed(2) + "% | " + received + " bytes out of " + total + " bytes.");
    },
    onFinished: (received, total) => {

      global.win.setProgressBar(0);

      if (progressBar) {
        progressBar.setCompleted();
      }

      unZip();

      console.log('Hurray! Upgrades file has been downloaded!');
    }

  });
}

function unZip() {


  var zip_file = 'offline_update_eav.zip';
  var dest_path = 'D:\\Temp\\eset_updates\\';
  var zip_file_path = global.path.join(dest_path, zip_file);

  unZipService.extractFiles({
    file_name: zip_file_path,
    dest_path: dest_path,
    onProgress: (file_index, files_count) => {

      if (!progressBar) {

        progressBar = new ProgressBar({
          indeterminate: false,
          text: 'Preparing data...',
          title: 'Wait please, archive is unzipping...',
          initialValue: 0,
          maxValue: files_count,
          browserWindow: {
            parent: global.win,
            width: 750,
            icon: 'assets/icon.png',
            webPreferences: {
              nodeIntegration: true
            }
          }
        }, global.app);

        progressBar
          .on('completed', function () {

            console.log(`Progress: Unzip is completed.`);
            progressBar.detail = 'Unzip is completed.';
            setTimeout(function () {
              progressBar.close();
              progressBar = null;
            }, 1000);

          })
          .on('aborted', function (value) {
            console.info(`aborted... ${value}`);
          })
          .on('progress', function (value) {
            console.log(`Progress: Value ${value} out of ${progressBar.getOptions().maxValue}...`);
            setTimeout(function () {
            progressBar.detail = `Progress: ${((value * 100) / progressBar.getOptions().maxValue).toFixed(2)} %. Downloaded ${value} out of ${progressBar.getOptions().maxValue} ...`;
          }, 500);
          });
      }

      var percentage = (file_index * 100) / files_count;
      global.win.setProgressBar(percentage / 100);

      if (!progressBar.isCompleted()) {
        progressBar.text = 'Extract files...';
        progressBar.value = file_index;
      }

    },
    onExtract: (log, file_name) => {

      var del_result = findRemoveSync(dest_path, { files: zip_file })

      global.win.setProgressBar(0);
      if (progressBar) {
        progressBar.setCompleted();
      }

      console.log('onExtract .zip been deleted => ' + JSON.stringify(del_result));

      const options = {
        type: 'info',
        buttons: ['Ok, great!'],
        defaultId: 2000,
        title: 'Complete',
        message: 'Updates downloaded and extracted.',
        detail: 'It is excellent! '
      };

      dialog.showMessageBox(global.win, options, (response, checkboxChecked) => {
      });
    }
  });

}

