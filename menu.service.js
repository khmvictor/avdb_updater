const global = require('./global');

class MenuService {

  showAboutMessage() {
    const options = {
      type: 'info',
      buttons: ['OK'],
      title: 'About program',
      message: 'Manual update ESET databases',
      detail:
        '\nUpdate ESET antivirus databease to current state\n' +
        'from custom resources.\n\n' +
        'Version: 1.0.1'
    };
    global.dialog.showMessageBox(null, options, (response) => {
      console.log(response);
    });
  }

  closeApp() {
    global.win.close();
  }


  hideToTray() {
    global.win.minimize();
  }

}
module.exports.menuService = new MenuService();
