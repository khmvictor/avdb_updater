require('dotenv').config();

const path = require('path');
const { notarize } = require('electron-notarize');

exports.default = async function notarizing(context) {
  const { electronPlatformName, appOutDir } = context;
  if (electronPlatformName !== 'darwin') {
    return;
  }

  const appName = context.packager.appInfo.productFilename;
  const appPath = path.join(appOutDir, `${appName}.app`);

  const appleId = process.env.APPLEID;
  const appleIdPassword = process.env.APPLEIDPASSWORD;

  try {
    await notarize({
      appBundleId: 'com.choones.choones',
      appPath: appPath,
      appleId: appleId,
      appleIdPassword: appleIdPassword
    });
  } catch (e) {
    console.log(e);
  }
};
