var request = require('request');
const global = require('./global');
const findRemoveSync = require('find-remove');

class DownloadService {

    downloadFile(download_config) {

        var received_bytes = 0;
        var total_bytes = 0;

        var req = request({
            method: 'GET',
            uri: download_config.db_ulr + download_config.file_name
        });

        var lpath = download_config.local_folder + download_config.file_name;

        try {
            if (!global.fs.existsSync(download_config.local_folder)) {
                global.fs.mkdirSync(download_config.local_folder);
                console.log('Directory has been created => ' + download_config.local_folder);
            } else {
                var result = findRemoveSync(download_config.local_folder, { extensions: ['.ver', '.zip', '.nup'] });
                console.log('Files  deleted => ' + JSON.stringify(result));
            }
        } catch (err) {
            console.log('Unable to create directory => ' + JSON.stringify(error));
            throw err;
        }

        var out = global.fs.createWriteStream(lpath);
        req.pipe(out);

        req.on('response', function (data) {
            // Change the total bytes value to get progress later.
            total_bytes = parseInt(data.headers['content-length']);

            // downloading is started
            if (download_config.hasOwnProperty("onStarted")) {
                download_config.onStarted(total_bytes);
            };
        });

        // Get progress if callback exists
        if (download_config.hasOwnProperty("onProgress")) {
            req.on('data', function (chunk) {
                // Update the received bytes
                received_bytes += chunk.length;

                download_config.onProgress(received_bytes, total_bytes);
            });
        } else {
            req.on('data', function (chunk) {
                // Update the received bytes
                received_bytes += chunk.length;
            });
        }

        req.on('end', () => {
            if (download_config.hasOwnProperty("onFinished")) {
                download_config.onFinished(received_bytes, total_bytes);
            }
        });
    }

}

module.exports.downloadService = new DownloadService();