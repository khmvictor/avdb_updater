

const { menuService } = require('./menu.service');
const global = require('./global');

const contextMenu = [
  {
    label: 'Restore App',
    accelerator: 'Alt+R',
    click: function () {
      global.win.show();
    }
  },
  {
    label: 'Toggle DevTools',
    accelerator: 'Alt+Command+I',
    click: function () {
      global.win.show();
      global.win.toggleDevTools();
    }
  },
  {
    label: 'Close program',
    accelerator: 'ALt+F4',
    selector: 'terminate:',
  }
];

const templateMenu = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Close program',
        axelerator: 'ALt+F4',
        click: () => { menuService.closeApp(); }
      },
      {
        label: 'Hide to tray',
        axelerator: 'ALt+H',
        click: () => { menuService.hideToTray(); }
      }
    ]
  },
  {
    label: 'About',
    submenu: [
      {
        label: 'About program',
        axelerator: 'ALt+H',
        click: () => { menuService.showAboutMessage(); }
      },
    ]
  },
  {
    label: 'Help',
    submenu: [
      {
        label: 'Reload',
        role: 'reload'
      },
      { type: 'separator' },
      { label: 'Version 1.0.0-alpha.2', enabled: 'FALSE' }
    ]
  }

]

module.exports = {
  templateMenu: templateMenu,
  contextMenu: contextMenu
};