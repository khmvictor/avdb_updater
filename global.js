/* Start global constants */
exports.app = require('electron').app;
exports.BrowserWindow = require('electron').BrowserWindow;
exports.Menu = require('electron').Menu;
exports.dialog = require('electron').dialog;
exports.ipcMain = require('electron').ipcMain;
exports.shell = require('electron').shell;
exports.prompt = require('electron-prompt');
exports.fs = require('fs');
exports.path = require('path');
/* End global constants */

/* Start global variables */
exports.dbPath = '';
exports.archiveFileName = '';
exports.win = '';
/* End global variables */
