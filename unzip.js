const global = require('./global');

var DecompressZip = require('decompress-zip');

class UnZipService {

    extractFiles(unzip_config) {

        var unzipper = new DecompressZip(unzip_config.file_name)

        unzipper.on('error', function (err) {
            console.log('Caught an error => ' + JSON.stringify(err));
        });

        unzipper.on('extract', function (log, file_name) {

            //console.log('Finished extracting => ' + JSON.stringify(log));

            if (unzip_config.hasOwnProperty("onExtract")) {
                unzip_config.onExtract(log, unzip_config.file_name);
            }
        });

        unzipper.on('progress', function (fileIndex, fileCount) {

            console.log('Extracted file  => ' + (fileIndex + 1) + ' of ' + fileCount);

            if (unzip_config.hasOwnProperty("onProgress")) {
                unzip_config.onProgress(fileIndex + 1, fileCount);
            }
        });

        unzipper.extract({
            path: unzip_config.dest_path
        });
    }

}

module.exports.unZipService = new UnZipService();